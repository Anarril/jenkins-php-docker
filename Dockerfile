# Official images are cool.
FROM jenkinsci/blueocean:latest
MAINTAINER Johan van Overbeeke <anarril@xs4all.nl>

# Jenkins is using jenkins user, we need root to install things.
USER root

RUN apk update && apk add -y wget unzip zip

RUN mkdir -p /tmp/WEB-INF/plugins

# Install required jenkins plugins.
RUN curl -SL "https://gitlab.com/Anarril/jenkins-php-docker/raw/master/install-jenkins-plugins.sh" -o install-jenkins-plugins \
 && chmod +x install-jenkins-plugins \
 && ./install-jenkins-plugins \
  checkstyle \
  cloverphp \
  crap4j \
  dry \
  htmlpublisher \
  jdepend \
  plot \
  pmd \
  violations \
  xunit \
  git-client \
  scm-api \
  git \
  bitbucket \
  publish-over-ssh \
  greenballs \
  htmlpublisher \
  workflow-aggregator \
  docker-build-publish \
  maven-plugin \
  analysis-core \
  pipeline-stage-view \
  publish-over \
  lockable-resources \
  simple-theme-plugin

# Install php packages.
RUN apk update && apk add -f \
 install \
 php7 \
 php7-dev \
 php-curl \
 php-json \
 php-phar \
 php-iconv \
 php-openssl \
 php-dom \
 php-tokenizer \
 php-pdo \
 php-intl \
 php-simplexml \
 php-xmlwriter \
 php-mbstring \
 curl \
 php-pear \
 ant \
 build-base

# Install docker
RUN apk update && apk add -f install docker

# Create a jenkins "HOME" for composer files.
RUN mkdir /home/jenkins
RUN chown jenkins:jenkins /home/jenkins

USER jenkins

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/jenkins

# Install required php tools.
RUN /home/jenkins/composer.phar --working-dir="/home/jenkins" -n require \
 phing/phing:2.* \
 notfloran/phing-composer-security-checker:~1.0 \
 phploc/phploc:* \
 phpunit/phpunit:~4.0 \
 pdepend/pdepend:~2.0 \
 phpmd/phpmd:~2.2 \
 sebastian/phpcpd:* \
 squizlabs/php_codesniffer:* \
 mayflower/php-codebrowser:~1.1 \
 codeception/codeception:* \
 jakub-onderka/php-parallel-lint
